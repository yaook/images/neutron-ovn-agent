diff --git a/neutron/agent/ovn/metadata/agent.py b/neutron/agent/ovn/metadata/agent.py
index 5541c045be..623980687f 100644
--- a/neutron/agent/ovn/metadata/agent.py
+++ b/neutron/agent/ovn/metadata/agent.py
@@ -40,6 +40,11 @@ from neutron.common import utils
 from neutron.conf.agent.database import agents_db
 from neutron.conf.plugins.ml2.drivers.ovn import ovn_conf as config
 
+from opentelemetry import trace
+from opentelemetry.exporter.jaeger.thrift import JaegerExporter
+from opentelemetry.sdk.resources import SERVICE_NAME, Resource
+from opentelemetry.sdk.trace import TracerProvider
+from opentelemetry.sdk.trace.export import BatchSpanProcessor
 
 LOG = log.getLogger(__name__)
 agents_db.register_db_agents_opts()
@@ -262,6 +267,25 @@ class MetadataAgent(object):
             resource_type='metadata')
         self._sb_idl = None
         self._post_fork_event = threading.Event()
+        jaeger_host = config.get_jaeger_hostname()
+        if jaeger_host:
+            resource = Resource(attributes={
+                SERVICE_NAME: "neutron-ovn-metadata-agent"
+            })
+
+            LOG.debug("Jaeger enabled, Host %s:%d",
+                    jaeger_host, config.get_jaeger_port())
+
+            jaeger_exporter = JaegerExporter(
+                agent_host_name=jaeger_host,
+                agent_port=config.get_jaeger_port(),
+                udp_split_oversized_batches=True
+            )
+
+            provider = TracerProvider(resource=resource)
+            processor = BatchSpanProcessor(jaeger_exporter)
+            provider.add_span_processor(processor)
+            trace.set_tracer_provider(provider)
 
     @property
     def sb_idl(self):
diff --git a/neutron/agent/ovn/metadata/server.py b/neutron/agent/ovn/metadata/server.py
index 3431735dff..b409c5d5d2 100644
--- a/neutron/agent/ovn/metadata/server.py
+++ b/neutron/agent/ovn/metadata/server.py
@@ -29,6 +29,11 @@ from oslo_config import cfg
 from oslo_log import log as logging
 import requests
 import webob
+from opentelemetry import trace
+from opentelemetry.trace.propagation.tracecontext import TraceContextTextMapPropagator
+
+
+tracer = trace.get_tracer(__name__)
 
 
 LOG = logging.getLogger(__name__)
@@ -76,6 +81,7 @@ class MetadataProxyHandler(object):
         # Now IDL connections can be safely used.
         self._post_fork_event.set()
 
     @webob.dec.wsgify(RequestClass=webob.Request)
+    @tracer.start_as_current_span("__call__")
     def __call__(self, req):
         try:
@@ -94,9 +100,15 @@ class MetadataProxyHandler(object):
             explanation = str(msg)
             return webob.exc.HTTPInternalServerError(explanation=explanation)
 
+    @tracer.start_as_current_span("_get_instance_and_project_id")
     def _get_instance_and_project_id(self, req):
         remote_address = req.headers.get('X-Forwarded-For')
         datapath_id = req.headers.get('X-OVN-Network-ID')
+        span = trace.get_current_span()
+        span.set_attributes({
+            "remote_address": remote_address,
+            "datapath_id": datapath_id,
+        })
 
         ports = self.sb_idl.get_network_port_bindings_by_ip(datapath_id,
                                                             remote_address)
@@ -118,7 +130,9 @@ class MetadataProxyHandler(object):
 
         return None, None
 
+    @tracer.start_as_current_span("_proxy_request")
     def _proxy_request(self, instance_id, tenant_id, req):
+        span = trace.get_current_span()
         headers = {
             'X-Forwarded-For': req.headers.get('X-Forwarded-For'),
             'X-Instance-ID': instance_id,
@@ -126,6 +140,8 @@ class MetadataProxyHandler(object):
             'X-Instance-ID-Signature': common_utils.sign_instance_id(
                 self.conf, instance_id)
         }
+        span.set_attributes(headers)
+        TraceContextTextMapPropagator().inject(headers)
 
         nova_host_port = ipv6_utils.valid_ipv6_url(
             self.conf.nova_metadata_host,
diff --git a/neutron/conf/plugins/ml2/drivers/ovn/ovn_conf.py b/neutron/conf/plugins/ml2/drivers/ovn/ovn_conf.py
index 0b0be8f56a..9bd81dde7f 100644
--- a/neutron/conf/plugins/ml2/drivers/ovn/ovn_conf.py
+++ b/neutron/conf/plugins/ml2/drivers/ovn/ovn_conf.py
@@ -222,6 +222,12 @@ ovn_opts = [
                        'order to disable ``stateful-security-group`` API '
                        'extension as ``allow-stateless`` keyword is only '
                        'supported by OVN >= 21.06.')),
+    cfg.StrOpt('jaeger_hostname',
+                default=None,
+                help=_('Configure the jaeger hostname')),
+    cfg.IntOpt('jaeger_port',
+                default=6831,
+                help=_('Configure the jaeger port')),
 ]
 
 
@@ -335,3 +341,11 @@ def is_igmp_snooping_enabled():
 
 def is_ovn_dhcp_disabled_for_baremetal():
     return cfg.CONF.ovn.disable_ovn_dhcp_for_baremetal_ports
+
+
+def get_jaeger_hostname():
+    return cfg.CONF.ovn.jaeger_hostname
+
+
+def get_jaeger_port():
+    return cfg.CONF.ovn.jaeger_port
\ No newline at end of file
diff --git a/neutron/plugins/ml2/drivers/ovn/mech_driver/ovsdb/impl_idl_ovn.py b/neutron/plugins/ml2/drivers/ovn/mech_driver/ovsdb/impl_idl_ovn.py
index a1b9f22fe1..69811f19bd 100644
--- a/neutron/plugins/ml2/drivers/ovn/mech_driver/ovsdb/impl_idl_ovn.py
+++ b/neutron/plugins/ml2/drivers/ovn/mech_driver/ovsdb/impl_idl_ovn.py
@@ -41,7 +41,9 @@ from neutron.plugins.ml2.drivers.ovn.mech_driver.ovsdb import commands as cmd
 from neutron.plugins.ml2.drivers.ovn.mech_driver.ovsdb import ovsdb_monitor
 from neutron.plugins.ml2.drivers.ovn.mech_driver.ovsdb import worker
 from neutron.services.portforwarding import constants as pf_const
+from opentelemetry import trace
 
+tracer = trace.get_tracer(__name__)
 
 LOG = log.getLogger(__name__)
 
@@ -938,6 +940,7 @@ class OvsdbSbOvnIdl(sb_impl_idl.OvnSbApiIdlImpl, Backend):
         return cmd.UpdateChassisExtIdsCommand(
             self, chassis, {desc_key: description}, if_exists=False)
 
+    @tracer.start_as_current_span("get_network_port_bindings_by_ip")
     def get_network_port_bindings_by_ip(self, datapath, ip_address, mac=None):
         # TODO(twilson) It would be useful to have a db_find that takes a
         # comparison function
diff --git a/requirements.txt b/requirements.txt
index 2b2d62fbb9..e51b3b04c0 100644
--- a/requirements.txt
+++ b/requirements.txt
@@ -63,3 +63,6 @@ os-vif>=3.1.0 # Apache-2.0
 futurist>=1.2.0 # Apache-2.0
 tooz>=1.58.0 # Apache-2.0
 wmi>=1.4.9;sys_platform=='win32'  # MIT
+opentelemetry-api>=1.15.0
+opentelemetry-sdk>=1.15.0
+opentelemetry-exporter-jaeger>=1.15.0
