From 0757b199b0099898692f54e401c542dc8d3ba6dc Mon Sep 17 00:00:00 2001
From: Luca Czesla <Luca.Czesla@mail.schwarz>
Date: Thu, 8 Aug 2024 13:55:44 +0200
Subject: [PATCH] feat: add tracing for neutron-ovn-metadata-agent

Change-Id: I51f92e43a42c476f516b12257bcf709d99d9a239
---
 neutron/agent/ovn/metadata/agent.py           | 25 +++++++++++++++++++
 neutron/agent/ovn/metadata/server.py          | 17 ++++++++++++-
 .../conf/plugins/ml2/drivers/ovn/ovn_conf.py  | 12 +++++++++
 .../ovn/mech_driver/ovsdb/impl_idl_ovn.py     |  5 ++++
 requirements.txt                              |  3 +++
 5 files changed, 61 insertions(+), 1 deletion(-)

diff --git a/neutron/agent/ovn/metadata/agent.py b/neutron/agent/ovn/metadata/agent.py
index dde7adf2ae..e043c0f52e 100644
--- a/neutron/agent/ovn/metadata/agent.py
+++ b/neutron/agent/ovn/metadata/agent.py
@@ -40,6 +40,12 @@ from neutron.common import utils
 from neutron.conf.agent.database import agents_db
 from neutron.conf.plugins.ml2.drivers.ovn import ovn_conf as config
 
+from opentelemetry import trace
+from opentelemetry.exporter.jaeger.thrift import JaegerExporter
+from opentelemetry.sdk.resources import SERVICE_NAME, Resource
+from opentelemetry.sdk.trace import TracerProvider
+from opentelemetry.sdk.trace.export import BatchSpanProcessor
+
 
 LOG = log.getLogger(__name__)
 agents_db.register_db_agents_opts()
@@ -230,6 +236,25 @@ class MetadataAgent(object):
             resource_type='metadata')
         self._sb_idl = None
         self._post_fork_event = threading.Event()
+        jaeger_host = config.get_jaeger_hostname()
+        if jaeger_host:
+            resource = Resource(attributes={
+                SERVICE_NAME: "neutron-ovn-metadata-agent"
+            })
+
+            LOG.debug("Jaeger enabled, Host %s:%d",
+                    jaeger_host, config.get_jaeger_port())
+
+            jaeger_exporter = JaegerExporter(
+                agent_host_name=jaeger_host,
+                agent_port=config.get_jaeger_port(),
+                udp_split_oversized_batches=True
+            )
+
+            provider = TracerProvider(resource=resource)
+            processor = BatchSpanProcessor(jaeger_exporter)
+            provider.add_span_processor(processor)
+            trace.set_tracer_provider(provider)
 
     @property
     def sb_idl(self):
diff --git a/neutron/agent/ovn/metadata/server.py b/neutron/agent/ovn/metadata/server.py
index 0b756f546b..71177b686e 100644
--- a/neutron/agent/ovn/metadata/server.py
+++ b/neutron/agent/ovn/metadata/server.py
@@ -31,6 +31,11 @@ from oslo_log import log as logging
 from oslo_utils import encodeutils
 import requests
 import webob
+from opentelemetry import trace
+from opentelemetry.trace.propagation.tracecontext import TraceContextTextMapPropagator
+
+
+tracer = trace.get_tracer(__name__)
 
 
 LOG = logging.getLogger(__name__)
@@ -79,6 +84,7 @@ class MetadataProxyHandler(object):
         self._post_fork_event.set()
 
     @webob.dec.wsgify(RequestClass=webob.Request)
+    @tracer.start_as_current_span("__call__")
     def __call__(self, req):
         try:
             LOG.debug("Request: %s", req)
@@ -96,10 +102,15 @@ class MetadataProxyHandler(object):
             explanation = str(msg)
             return webob.exc.HTTPInternalServerError(explanation=explanation)
 
+    @tracer.start_as_current_span("_get_instance_and_project_id")
     def _get_instance_and_project_id(self, req):
         remote_address = req.headers.get('X-Forwarded-For')
         datapath_id = req.headers.get('X-OVN-Network-ID')
-
+        span = trace.get_current_span()
+        span.set_attributes({
+            "remote_address": remote_address,
+            "datapath_id": datapath_id,
+        })
         ports = self.sb_idl.get_network_port_bindings_by_ip(datapath_id,
                                                             remote_address)
         num_ports = len(ports)
@@ -120,13 +131,17 @@ class MetadataProxyHandler(object):
 
         return None, None
 
+    @tracer.start_as_current_span("_proxy_request")
     def _proxy_request(self, instance_id, tenant_id, req):
+        span = trace.get_current_span()
         headers = {
             'X-Forwarded-For': req.headers.get('X-Forwarded-For'),
             'X-Instance-ID': instance_id,
             'X-Tenant-ID': tenant_id,
             'X-Instance-ID-Signature': self._sign_instance_id(instance_id)
         }
+        span.set_attributes(headers)
+        TraceContextTextMapPropagator().inject(headers)
 
         nova_host_port = ipv6_utils.valid_ipv6_url(
             self.conf.nova_metadata_host,
diff --git a/neutron/conf/plugins/ml2/drivers/ovn/ovn_conf.py b/neutron/conf/plugins/ml2/drivers/ovn/ovn_conf.py
index 00d3640eb7..ab569f92bd 100644
--- a/neutron/conf/plugins/ml2/drivers/ovn/ovn_conf.py
+++ b/neutron/conf/plugins/ml2/drivers/ovn/ovn_conf.py
@@ -213,6 +213,12 @@ ovn_opts = [
                        '(VNIC type "baremetal"). This alllow operators to '
                        'plug their own DHCP server of choice for PXE booting '
                        'baremetal nodes. Defaults to False.')),
+    cfg.StrOpt('jaeger_hostname',
+                default=None,
+                help=_('Configure the jaeger hostname')),
+    cfg.IntOpt('jaeger_port',
+                default=6831,
+                help=_('Configure the jaeger port')),
 ]
 
 
@@ -326,3 +332,9 @@ def is_igmp_snooping_enabled():
 
 def is_ovn_dhcp_disabled_for_baremetal():
     return cfg.CONF.ovn.disable_ovn_dhcp_for_baremetal_ports
+
+def get_jaeger_hostname():
+    return cfg.CONF.ovn.jaeger_hostname
+
+def get_jaeger_port():
+    return cfg.CONF.ovn.jaeger_port
diff --git a/neutron/plugins/ml2/drivers/ovn/mech_driver/ovsdb/impl_idl_ovn.py b/neutron/plugins/ml2/drivers/ovn/mech_driver/ovsdb/impl_idl_ovn.py
index 9e82034e45..24c1283319 100644
--- a/neutron/plugins/ml2/drivers/ovn/mech_driver/ovsdb/impl_idl_ovn.py
+++ b/neutron/plugins/ml2/drivers/ovn/mech_driver/ovsdb/impl_idl_ovn.py
@@ -40,8 +40,12 @@ from neutron.plugins.ml2.drivers.ovn.mech_driver.ovsdb import commands as cmd
 from neutron.plugins.ml2.drivers.ovn.mech_driver.ovsdb import ovsdb_monitor
 from neutron.plugins.ml2.drivers.ovn.mech_driver.ovsdb import worker
 from neutron.services.portforwarding import constants as pf_const
+from opentelemetry import trace
 
 
+
+tracer = trace.get_tracer(__name__)
+
 LOG = log.getLogger(__name__)
 
 
@@ -928,6 +932,7 @@ class OvsdbSbOvnIdl(sb_impl_idl.OvnSbApiIdlImpl, Backend):
         return cmd.UpdateChassisExtIdsCommand(
             self, chassis, {desc_key: description}, if_exists=False)
 
+    @tracer.start_as_current_span("get_network_port_bindings_by_ip")
     def get_network_port_bindings_by_ip(self, datapath, ip_address, mac=None):
         # TODO(twilson) It would be useful to have a db_find that takes a
         # comparison function
diff --git a/requirements.txt b/requirements.txt
index 83450edd8f..b5d21d7777 100644
--- a/requirements.txt
+++ b/requirements.txt
@@ -63,3 +63,6 @@ os-vif>=1.15.1 # Apache-2.0
 futurist>=1.2.0 # Apache-2.0
 tooz>=1.58.0 # Apache-2.0
 wmi>=1.4.9;sys_platform=='win32'  # MIT
+opentelemetry-api>=1.15.0
+opentelemetry-sdk>=1.15.0
+opentelemetry-exporter-jaeger>=1.15.0
-- 
2.20.1

