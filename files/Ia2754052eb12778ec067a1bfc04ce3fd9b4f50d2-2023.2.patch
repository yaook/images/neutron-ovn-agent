From 1f9a1006719132daec58c029b9262b0e2fe18dad Mon Sep 17 00:00:00 2001
From: Rodolfo Alonso Hernandez <ralonsoh@redhat.com>
Date: Wed, 3 Jul 2024 14:03:45 +0000
Subject: [PATCH] Change the "get_metadata_port" name and input variable

This method retrieves the metadata port using the SB datapath UUID,
not the network name.

Trivial-Fix

Change-Id: Ia2754052eb12778ec067a1bfc04ce3fd9b4f50d2
---
 neutron/agent/ovn/metadata/agent.py           | 12 +++------
 .../ovn/mech_driver/ovsdb/impl_idl_ovn.py     |  4 +--
 .../ovn/mech_driver/ovsdb/test_impl_idl.py    | 25 ++++++++++++++-----
 .../unit/agent/ovn/metadata/test_agent.py     |  8 +++---
 4 files changed, 29 insertions(+), 20 deletions(-)

diff --git a/neutron/agent/ovn/metadata/agent.py b/neutron/agent/ovn/metadata/agent.py
index 999ad3cd65..c4fe13424a 100644
--- a/neutron/agent/ovn/metadata/agent.py
+++ b/neutron/agent/ovn/metadata/agent.py
@@ -588,14 +588,10 @@ class MetadataAgent(object):
         net_name = ovn_utils.get_network_name_from_datapath(datapath)
         datapath_uuid = str(datapath.uuid)
 
-        metadata_port = self.sb_idl.get_metadata_port_network(datapath_uuid)
-        # If there's no metadata port or it doesn't have a MAC or IP
-        # addresses, then tear the namespace down if needed. This might happen
-        # when there are no subnets yet created so metadata port doesn't have
-        # an IP address.
-        if not (metadata_port and metadata_port.mac and
-                metadata_port.external_ids.get(
-                    ovn_const.OVN_CIDRS_EXT_ID_KEY, None)):
+        metadata_port = self.sb_idl.get_metadata_port(datapath_uuid)
+        # If there's no metadata port or it doesn't have a MAC address, then
+        # tear the namespace down if needed.
+        if not (metadata_port and metadata_port.mac):
             LOG.debug("There is no metadata port for network %s or it has no "
                       "MAC or IP addresses configured, tearing the namespace "
                       "down if needed", net_name)
diff --git a/neutron/plugins/ml2/drivers/ovn/mech_driver/ovsdb/impl_idl_ovn.py b/neutron/plugins/ml2/drivers/ovn/mech_driver/ovsdb/impl_idl_ovn.py
index 094f3fbf1f..032d257e16 100644
--- a/neutron/plugins/ml2/drivers/ovn/mech_driver/ovsdb/impl_idl_ovn.py
+++ b/neutron/plugins/ml2/drivers/ovn/mech_driver/ovsdb/impl_idl_ovn.py
@@ -917,10 +917,10 @@ class OvsdbSbOvnIdl(sb_impl_idl.OvnSbApiIdlImpl, Backend):
                      card_serial_number)
         raise RuntimeError(msg)
 
-    def get_metadata_port_network(self, network):
+    def get_metadata_port(self, datapath_uuid):
         # TODO(twilson) This function should really just take a Row/RowView
         try:
-            dp = self.lookup('Datapath_Binding', uuid.UUID(network))
+            dp = self.lookup('Datapath_Binding', uuid.UUID(datapath_uuid))
         except idlutils.RowNotFound:
             return None
         cmd = self.db_find_rows('Port_Binding', ('datapath', '=', dp),
diff --git a/neutron/tests/functional/plugins/ml2/drivers/ovn/mech_driver/ovsdb/test_impl_idl.py b/neutron/tests/functional/plugins/ml2/drivers/ovn/mech_driver/ovsdb/test_impl_idl.py
index 1d7631137e..b790f59bb3 100644
--- a/neutron/tests/functional/plugins/ml2/drivers/ovn/mech_driver/ovsdb/test_impl_idl.py
+++ b/neutron/tests/functional/plugins/ml2/drivers/ovn/mech_driver/ovsdb/test_impl_idl.py
@@ -126,10 +126,23 @@ class TestSbApi(BaseOvnIdlTest):
         row_event.wait()
         return port.result, row_event.row
 
-    def test_get_metadata_port_network(self):
+    def test_get_metadata_port(self):
         chassis, switch = self._add_switch(self.data['chassis'][0]['name'])
         port, binding = self._add_port_to_switch(switch)
-        result = self.api.get_metadata_port_network(str(binding.datapath.uuid))
+        result = self.api.get_metadata_port(str(binding.datapath.uuid))
+        self.assertEqual(binding, result)
+        self.assertEqual(binding.datapath.external_ids['logical-switch'],
+                         str(switch.uuid))
+        self.assertEqual(
+            port.external_ids[ovn_const.OVN_DEVICE_OWNER_EXT_ID_KEY],
+            constants.DEVICE_OWNER_DISTRIBUTED)
+
+    def test_get_metadata_port_other_non_metadata_port(self):
+        chassis, switch = self._add_switch(self.data['chassis'][0]['name'])
+        port, binding = self._add_port_to_switch(switch)
+        port_lbhm, binding_port_lbhm = self._add_port_to_switch(
+            switch, device_owner=ovn_const.OVN_LB_HM_PORT_DISTRIBUTED)
+        result = self.api.get_metadata_port(str(binding.datapath.uuid))
         self.assertEqual(binding, result)
         self.assertEqual(binding.datapath.external_ids['logical-switch'],
                          str(switch.uuid))
@@ -153,9 +166,9 @@ class TestSbApi(BaseOvnIdlTest):
             port_lbhm.external_ids[ovn_const.OVN_DEVICE_OWNER_EXT_ID_KEY],
             ovn_const.OVN_LB_HM_PORT_DISTRIBUTED)
 
-    def test_get_metadata_port_network_missing(self):
+    def test_get_metadata_port_missing(self):
         val = str(uuid.uuid4())
-        self.assertIsNone(self.api.get_metadata_port_network(val))
+        self.assertIsNone(self.api.get_metadata_port(val))
 
     def _create_bound_port_with_ip(self):
         chassis, switch = self._add_switch(
diff --git a/neutron/tests/unit/agent/ovn/metadata/test_agent.py b/neutron/tests/unit/agent/ovn/metadata/test_agent.py
index 7a529d9d82..e15d66c2d8 100644
--- a/neutron/tests/unit/agent/ovn/metadata/test_agent.py
+++ b/neutron/tests/unit/agent/ovn/metadata/test_agent.py
@@ -289,7 +289,7 @@ class TestMetadataAgent(base.BaseTestCase):
             external_ids={'name': 'neutron-{}'.format(network_id)})
 
         with mock.patch.object(
-                self.agent.sb_idl, 'get_metadata_port_network',
+                self.agent.sb_idl, 'get_metadata_port',
                 return_value=None),\
             mock.patch.object(
                 self.agent, 'teardown_datapath') as tdp:
@@ -310,7 +310,7 @@ class TestMetadataAgent(base.BaseTestCase):
                                                  '10.204.0.10/29'})
 
         with mock.patch.object(
-                self.agent.sb_idl, 'get_metadata_port_network',
+                self.agent.sb_idl, 'get_metadata_port',
                 return_value=metadadata_port),\
             mock.patch.object(
                 self.agent, 'teardown_datapath') as tdp:
@@ -330,7 +330,7 @@ class TestMetadataAgent(base.BaseTestCase):
                                    external_ids={'neutron:cidrs':
                                                  '10.204.0.10/29'})
 
-        with mock.patch.object(self.agent.sb_idl, 'get_metadata_port_network',
+        with mock.patch.object(self.agent.sb_idl, 'get_metadata_port',
                     return_value=metadadata_port),\
                 mock.patch.object(self.agent.sb_idl, 'get_ports_on_chassis',
                     return_value=datapath_ports),\
@@ -360,7 +360,7 @@ class TestMetadataAgent(base.BaseTestCase):
                                                  metada_port_subnet_cidr},
                                    logical_port=metada_port_logical_port)
 
-        with mock.patch.object(self.agent.sb_idl, 'get_metadata_port_network',
+        with mock.patch.object(self.agent.sb_idl, 'get_metadata_port',
                 return_value=metadadata_port),\
             mock.patch.object(self.agent.sb_idl, 'get_ports_on_chassis',
                 return_value=datapath_ports):

