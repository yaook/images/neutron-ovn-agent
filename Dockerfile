##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

ARG release
ARG ovs_version

ARG LANG=C.UTF-8
ARG DEBIAN_FRONTEND=noninteractive

ARG BUILD_PACKAGES="autoconf build-essential git libtool pkg-config libffi-dev"
ARG REQUIRED_PACKAGES="curl haproxy iproute2 procps sudo tini"

ARG DESTDIR=/build

FROM python:3.11.6-slim-bookworm@sha256:cc758519481092eb5a4a5ab0c1b303e288880d59afc601958d19e95b300bc86b AS builder

ARG ovs_version

ARG LANG
ARG DEBIAN_FRONTEND

ARG BUILD_PACKAGES

ARG DESTDIR

RUN set -eux ; \
    apt update; \
    apt install -y \
        ${BUILD_PACKAGES}

COPY files/*.patch /

# A more recent version of the OVS python library is required for the neutron-ovn-metadata-agent.
# This is not intended to work nor tested to work with an Neutron OVS backend!
RUN set -eux ; \
    git clone -b ${ovs_version} https://github.com/openvswitch/ovs.git /ovs --depth 1; \
    cd /ovs; \
    patch -p1 < /0001-python-index-references.patch; \
    ./boot.sh; \
    ./configure \
        --localstatedir=/var \
        --prefix=/usr \
        --sysconfdir=/etc \
        # We need shared linking for the ovs._json python lib
        # With that lib OVS parses json in C instead of python (huge speed improvement)
        --enable-shared; \
    gmake -j$(nproc); \
    make install DESTDIR=${DESTDIR}; \
    cp -r /ovs/python ${DESTDIR}


FROM python:3.11.6-slim-bookworm@sha256:cc758519481092eb5a4a5ab0c1b303e288880d59afc601958d19e95b300bc86b

ARG release

ARG BUILD_PACKAGES
ARG REQUIRED_PACKAGES

ARG DESTDIR

COPY --from=builder ${DESTDIR}/ /
COPY files/*-runner.sh /
COPY files/*.patch /

RUN set -eux ; \
    apt update; \
    apt install -y \
        ${BUILD_PACKAGES} \
        ${REQUIRED_PACKAGES}; \
    # A more recent version of the OVS python library is required for the neutron-ovn-metadata-agent.
    # This is not intended to work nor tested to work with an Neutron OVS backend!
    cd /python/; \
    python3 setup.py build; \
    python3 setup.py install; \
    cd /; rm -rf /python; \
    for repo in requirements neutron; do \
        git clone "https://opendev.org/openstack/$repo.git" --depth 1 --branch "stable/${release}" || \
        git clone "https://opendev.org/openstack/$repo.git" --depth 1 --branch "${release}-eol" || \
        git clone "https://opendev.org/openstack/$repo.git" --depth 1 --branch "unmaintained/${release}" \
        ; \
    done; \
    \
    sed -i '/^neutron===.*/d' /requirements/upper-constraints.txt; \
    # As a newer OVS version is necessary, the upper constraint of python-ovs has to be removed.
    sed -i '/^ovs===.*/d' /requirements/upper-constraints.txt; \
    # We need at least pyroute 0.6.10 to work around https://bugs.launchpad.net/neutron/+bug/1995735
    sed -i 's/^\(pyroute2.*\)===0.6.6/\1===0.6.10/' /requirements/upper-constraints.txt; \
    # needed for python 3.11
    # https://github.com/GrahamDumpleton/wrapt/issues/196
    sed -i 's/^\(wrapt.*\)===1.13.3/\1===1.14.0/' /requirements/upper-constraints.txt; \
    # https://github.com/sumerc/yappi/issues/104
    sed -i 's/^\(yappi.*\)===1.3.[0-9]\+/\1===1.4.0/' /requirements/upper-constraints.txt; \
    # https://github.com/python-greenlet/greenlet/issues/295#issuecomment-1078891696
    sed -i 's/^\(greenlet.*\)===1.1.2/\1===2.0.2/' /requirements/upper-constraints.txt; \
    # raise requirements for tracing compatibility in zed
    sed -i 's/^\(grpcio.*\)===1.47.0/grpcio===1.49.1/' /requirements/upper-constraints.txt; \
    # needed for ovsdbapp >= 2.8.0
    sed -i '/^netaddr===.*/d' /requirements/upper-constraints.txt; \
    sed -i 's/^ovsdbapp.*/ovsdbapp===2.8.0/' /requirements/upper-constraints.txt; \
    cd /neutron; \
    if [ ${release} != "2024.1" ]; then \
      patch -p1 < /896251.patch; \
      # backport of https://review.opendev.org/c/openstack/neutron/+/923406
    fi; \
    patch -p1 < /"Ia2754052eb12778ec067a1bfc04ce3fd9b4f50d2-${release}.patch"; \
    patch -p1 < /"0001-fix-speed-of-metadata-requests-using-the-datapath-${release}.patch"; \
    patch -p1 < /0001-fix-catch-error-of-livenessProbe.patch; \
    if [ -f /"0001-feat-add-tracing-for-neutron-ovn-metadata-agent-${release}.patch" ]; then \
        patch -p1 < /"0001-feat-add-tracing-for-neutron-ovn-metadata-agent-${release}.patch"; \
    fi; \
    cd /; \
    pip3 install --no-cache-dir -U pip; \
    pip3 install --no-cache-dir setuptools==45; `# WORKAROUND for https://github.com/pallets/markupsafe/issues/116` \
    pip3 install --no-cache-dir wheel; \
    pip3 install --no-cache-dir -c /requirements/upper-constraints.txt PyMySQL python-memcached pymemcache neutron/; \
    if [ ${release} != "2024.1" ]; then \
      patch -d /usr/local/lib/python3.11/site-packages -p1 < /0001-allow-multiple-indexes-on-the-same-table.patch ; \
      patch -d /usr/local/lib/python3.11/site-packages -p1 < /0002-add-datapath-from-port_binding-as-index.patch ; \
    fi; \
    rm -rf requirements neutron; \
    apt-get purge --autoremove -y \
        ${BUILD_PACKAGES}; \
    apt-get clean autoclean; \
    rm -rf /var/lib/apt/lists/* /root/.cache /var/lib/cache/*; \
    # /usr/local/etc/neutron/rootwrap.conf expects rootwrap filters under /usr/share/neutron/rootwrap
    mkdir -p /usr/share/neutron/rootwrap; \
    ln -s /usr/local/etc/neutron/rootwrap.d/rootwrap.filters /usr/share/neutron/rootwrap; \
    ovs-appctl --version

ENTRYPOINT ["tini", "--"]
